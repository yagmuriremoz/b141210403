﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(b141210403.Startup))]
namespace b141210403
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
